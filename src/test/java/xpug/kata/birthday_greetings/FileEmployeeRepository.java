package xpug.kata.birthday_greetings;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class FileEmployeeRepository implements EmployeeRepository {

    private String fileName;

    public FileEmployeeRepository(String fileName) {
        this.fileName=fileName;
    }

    @Override
    public List<Employee> findEmployeesBornOn(OurDate ourDate) throws IOException, ParseException {
        List <Employee> employeesWithBirthdayToday= new ArrayList<Employee>();
        BufferedReader in = new BufferedReader(new FileReader(fileName));
        String employeeRecord = "";
        employeeRecord = in.readLine(); // skip header
        System.out.println("Primera linea de archivo");
        while ((employeeRecord = in.readLine()) != null) {
            Employee employee = extractEmployee(employeeRecord);
            if (employee.isBirthday(ourDate)) {
                employeesWithBirthdayToday.add(employee);
            }
        }
        return employeesWithBirthdayToday;
    }

    private Employee extractEmployee(String str) throws ParseException {
        String[] employeeData = str.split(", ");
        String firstName = employeeData[1];
        String lastName = employeeData[0];
        String birthDate = employeeData[2];
        String email =employeeData[3];

        Employee employee = new Employee(firstName,lastName,birthDate,email);
        return employee;
    }

}
