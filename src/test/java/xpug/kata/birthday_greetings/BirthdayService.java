package xpug.kata.birthday_greetings;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import javax.mail.MessagingException;

public class BirthdayService {
	int numberOfGreetingsSent;

	private EmployeeRepository employeeRepository;
	private EmailService mail;

	public BirthdayService(EmployeeRepository employeeRepository, EmailService email) {
		this.employeeRepository = employeeRepository;
		this.mail = email;
	}

	public void sendGreetings(OurDate ourDate) throws IOException, ParseException, MessagingException {
		List<Employee> employees =  employeeRepository.findEmployeesBornOn(ourDate);
		for(int i = 0; i<employees.size();i++){
			mail.sendMessage("sender@here.com", employees.get(i));
		}
		
	}

	public static void main(String[] args) {
		EmployeeRepository employeesRepository = new FileEmployeeRepository("employee_data.txt");
		EmailService mail = new SMTPMailService("localhost", 25);
		BirthdayService service = new BirthdayService(employeesRepository,mail);

		try {
			service.sendGreetings(
					new OurDate("2008/10/08"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int quantityOfGreetingsSent() {
		return numberOfGreetingsSent;
	}
}
